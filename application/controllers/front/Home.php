<?php
class Home extends My_Controller {

	public function index() {
		echo 'controller -> home/index';
		$this->template->load('layout', $this->front_view);
	}

	public function busca() {
		echo 'controller -> home/busca';
		$this->template->load('layout', $this->front_view);
	}
}