<?php
class My_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$controlle = 'home';
		$view = 'index';
		
		if ($this->uri->segment(1) == 'front' || $this->uri->segment(1) == 'admin') {
			$controlle = $this->uri->segment(2, 'home');
			$view = $this->uri->segment(3, 'index');
		} else {
			$controlle = $this->uri->segment(1, 'home');
			$view = $this->uri->segment(2, 'index');
		}

		// monta a url para carregar os arquivos do front
		$this->front_view = 'front/' . $controlle . '/' . $view . '.phtml';

		// monta a url para carregar os arquivos do amdin
		$this->admin_view = 'admin/' . $controlle . '/' . $view . '.phtml';
	}
}